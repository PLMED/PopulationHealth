package edu.uky.pml.ph.collector;

import com.google.gson.Gson;
import edu.uky.pml.ph.models.InfectionUpdate;
import io.cresco.library.data.TopicType;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.UUID;

@SuppressWarnings({"unused", "WeakerAccess"})
public class IngestionEngine {
    private PluginBuilder pluginBuilder;
    private CLogger logger;
    private String instanceTag;
    private String elasticsearchHost;
    private int elasticsearchPort;
    private String elasticsearchProtocol;
    private String elasticsearchIndex;
    private String listenerId = null;

    public IngestionEngine(PluginBuilder pluginBuilder) {
        setPluginBuilder(pluginBuilder);
        this.logger = pluginBuilder.getLogger(IngestionEngine.class.getName(), CLogger.Level.Trace);
        logger.trace("InstanceEnginer()");
        setInstanceTag(UUID.randomUUID().toString());
        setElasticsearchHost(pluginBuilder.getConfig().getStringParam("es_host", "192.168.128.1"));
        setElasticsearchPort(pluginBuilder.getConfig().getIntegerParam("es_port", 9200));
        setElasticsearchProtocol(pluginBuilder.getConfig().getStringParam("es_protocol", "http"));
        setElasticsearchIndex(pluginBuilder.getConfig().getStringParam("es_index", "ukhc-infection-tracker"));
    }

    public void start() {
        logger.trace("start()");
        if (listenerId == null) {
            logger.trace("Building MessageListener");
            MessageListener ml = (Message msg) -> {
                try {
                    if (msg instanceof TextMessage) {
                        Gson gson = new Gson();
                        TextMessage textMessage = (TextMessage) msg;
                        InfectionUpdate infectionUpdate = gson.fromJson(textMessage.getText(), InfectionUpdate.class);
                        RestHighLevelClient client = new RestHighLevelClient(
                                RestClient.builder(
                                        new HttpHost(
                                                getElasticsearchHost(),
                                                getElasticsearchPort(),
                                                getElasticsearchProtocol()
                                        )
                                )
                        );
                        logger.trace("Flu cases: {}, HepA cases: {}, HepB cases: {}, HepC cases: {}, County: {}",
                                infectionUpdate.getFlu(), infectionUpdate.getHepA(), infectionUpdate.getHepB(),
                                infectionUpdate.getHepC(), infectionUpdate.getArea().county_name);
                        IndexRequest indexRequest = new IndexRequest(getElasticsearchIndex());
                        indexRequest.source(textMessage.getText(), XContentType.JSON);
                        client.index(indexRequest, RequestOptions.DEFAULT);
                        client.close();
                    }
                } catch (Exception e) {
                    if (logger != null) {
                        logger.error("listener - onMessage(): {}:{}", e.getClass().getCanonicalName(), e.getMessage());
                        logger.trace("listener - onMessage():\n" + ExceptionUtils.getStackTrace(e));
                    } else
                        e.printStackTrace();
                }
            };
            listenerId = pluginBuilder.getAgentService().getDataPlaneService().addMessageListener(
                    TopicType.AGENT,
                    ml,
                    String.format(
                            "%s='%s'",
                            InfectionUpdate.DATA_PLANE_IDENTIFIER_KEY,
                            InfectionUpdate.DATA_PLANE_IDENTIFIER_VALUE)
            );
            logger.trace("Created ingestion listener watching for [{}={}] and sending updates to [{}://{}:{}] using index [{}]",
                    InfectionUpdate.DATA_PLANE_IDENTIFIER_KEY, InfectionUpdate.DATA_PLANE_IDENTIFIER_VALUE,
                    getElasticsearchProtocol(), getElasticsearchHost(), getElasticsearchPort(), getElasticsearchIndex());
        }
    }

    public void stop() {
        if (listenerId != null) {
            logger.trace("Removing existing listener");
            pluginBuilder.getAgentService().getDataPlaneService().removeMessageListener(listenerId);
            listenerId = null;
        }
    }

    public PluginBuilder getPluginBuilder() {
        return pluginBuilder;
    }
    public void setPluginBuilder(PluginBuilder pluginBuilder) {
        this.pluginBuilder = pluginBuilder;
    }

    public String getInstanceTag() {
        return instanceTag;
    }
    public void setInstanceTag(String instanceTag) {
        this.instanceTag = instanceTag;
    }

    public String getElasticsearchHost() {
        return elasticsearchHost;
    }
    public void setElasticsearchHost(String elasticsearchHost) {
        this.elasticsearchHost = elasticsearchHost;
    }

    public int getElasticsearchPort() {
        return elasticsearchPort;
    }
    public void setElasticsearchPort(int elasticsearchPort) {
        this.elasticsearchPort = elasticsearchPort;
    }

    public String getElasticsearchProtocol() {
        return elasticsearchProtocol;
    }
    public void setElasticsearchProtocol(String elasticsearchProtocol) {
        this.elasticsearchProtocol = elasticsearchProtocol;
    }

    public String getElasticsearchIndex() {
        return elasticsearchIndex;
    }
    public void setElasticsearchIndex(String elasticsearchIndex) {
        this.elasticsearchIndex = elasticsearchIndex;
    }
}
