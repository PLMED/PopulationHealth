package edu.uky.pml.ph.models;

import java.util.ArrayList;
import java.util.List;

public class CensusArea {
    public Input input;
    public List<Result> results = new ArrayList<>();

    public class Input {
        double lat;
        double lon;
    }

    public class Result {
        public String block_fips;
        public double[] bbox;
        public String county_fips;
        public String county_name;
        public String state_fips;
        public String state_code;
        public String state_name;
        public int block_pop_2015;
        public String amt;
        public String bea;
        public String bta;
        public String cma;
        public String eag;
        public String ivm;
        public String mea;
        public String mta;
        public String pea;
        public String rea;
        public String rpc;
        public String vps;
    }

    @Override
    public String toString() {
        if (results.size() > 0)
            return String.format("%s [%s] County in %s",
                    results.get(0).county_name,
                    results.get(0).county_fips,
                    results.get(0).state_name
            );
        return "Unknown [Unknown] County in Unknown";
    }
}
