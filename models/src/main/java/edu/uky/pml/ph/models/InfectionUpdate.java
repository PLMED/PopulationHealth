package edu.uky.pml.ph.models;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
public class InfectionUpdate {
    public static final String DATA_PLANE_IDENTIFIER_KEY = "updateType";
    public static final String DATA_PLANE_IDENTIFIER_VALUE = "infection_tracker";

    private String date;
    private String siteId;

    private int flu;
    private int hepA;
    private int hepB;
    private int hepC;

    private CensusArea.Result area;

    public InfectionUpdate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");
        setDate(dateFormat.format(new Date()));
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getSiteId() {
        return siteId;
    }
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public int getFlu() {
        return flu;
    }
    public void setFlu(int flu) {
        this.flu = flu;
    }

    public int getHepA() {
        return hepA;
    }
    public void setHepA(int hepA) {
        this.hepA = hepA;
    }

    public int getHepB() {
        return hepB;
    }
    public void setHepB(int hepB) {
        this.hepB = hepB;
    }

    public int getHepC() {
        return hepC;
    }
    public void setHepC(int hepC) {
        this.hepC = hepC;
    }

    public CensusArea.Result getArea() {
        return area;
    }
    public void setArea(CensusArea.Result area) {
        this.area = area;
    }
}
