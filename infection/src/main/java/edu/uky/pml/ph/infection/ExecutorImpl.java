package edu.uky.pml.ph.infection;

import io.cresco.library.messaging.MsgEvent;
import io.cresco.library.plugin.Executor;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;

public class ExecutorImpl implements Executor {

    private PluginBuilder plugin;
    private CLogger logger;

    public ExecutorImpl(PluginBuilder pluginBuilder) {
        this.plugin = pluginBuilder;
        logger = plugin.getLogger(ExecutorImpl.class.getName(), CLogger.Level.Info);
    }

    @Override
    public MsgEvent executeCONFIG(MsgEvent incoming) {
        logger.info("Received CONFIG message");
        return null;
    }
    @Override
    public MsgEvent executeDISCOVER(MsgEvent incoming) {
        logger.info("Received DISCOVER message");
        return null;
    }
    @Override
    public MsgEvent executeERROR(MsgEvent incoming) {
        logger.info("Received ERROR message");
        return null;
    }
    @Override
    public MsgEvent executeINFO(MsgEvent incoming) {
        logger.info("Received INFO message");
        return null;
    }
    @Override
    public MsgEvent executeEXEC(MsgEvent incoming) {
        logger.info("Received EXEC message");
        return null;
    }
    @Override
    public MsgEvent executeWATCHDOG(MsgEvent incoming) {
        logger.info("Received WATCHDOG message");
        return null;
    }
    @Override
    public MsgEvent executeKPI(MsgEvent incoming) {
        logger.info("Received KPI message");
        return null;
    }
}