package edu.uky.pml.ph.infection;

import com.google.gson.Gson;
import edu.uky.pml.ph.models.CensusArea;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

@SuppressWarnings({"unused", "WeakerAccess"})
public class LocationResolver {
    private static final String RESOLVER_SCHEME = "https";
    private static final int RESOLVER_PORT = 443;
    private static final String RESOLVER_HOST = "geo.fcc.gov";
    private static final String AREA_ENDPOINT = "/api/census/area";

    private CLogger logger;
    private double latitude;
    private double longitude;
    private CensusArea area;

    public LocationResolver(PluginBuilder pluginBuilder) {
        this.logger = pluginBuilder.getLogger(LocationResolver.class.getName(), CLogger.Level.Trace);
        logger.trace("LocationResolver()");
        double lat = pluginBuilder.getConfig().getDoubleParam("location_latitude", 38.0299972);
        logger.trace("Using latitude: {}", lat);
        double lon = pluginBuilder.getConfig().getDoubleParam("location_longitude", -84.5114492);
        logger.trace("Using longitude: {}", lon);
        updateCoordinates(lat, lon);
    }

    public void updateCoordinates(double latitude, double longitude) {
        logger.trace("updateCoordinates({},{})", latitude, longitude);
        HttpClient httpclient;
        try {
            httpclient = HttpClientBuilder.create().build();
            HttpHost target = new HttpHost(RESOLVER_HOST, RESOLVER_PORT, RESOLVER_SCHEME);
            HttpGet getRequest = new HttpGet(String.format("%s?lat=%f&lon=%f&format=json", AREA_ENDPOINT, latitude, longitude));
            HttpResponse httpResponse = httpclient.execute(target, getRequest);
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                String json = EntityUtils.toString(entity, "UTF-8");
                Gson gson = new Gson();
                area = gson.fromJson(json, CensusArea.class);
            }
        } catch (Exception e) {
            if (logger != null) {
                logger.error("updateCoordinates(): {}:{}", e.getClass().getCanonicalName(), e.getMessage());
                logger.trace("updateCoordinates():\n" + ExceptionUtils.getStackTrace(e));
            } else
                e.printStackTrace();
        }
    }

    public CensusArea getArea() {
        return area;
    }

    public CensusArea.Result getAreaResult() {
        if (getArea() != null && getArea().results.size() > 0)
            return getArea().results.get(0);
        return null;
    }
}
