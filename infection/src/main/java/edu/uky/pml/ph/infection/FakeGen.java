package edu.uky.pml.ph.infection;

import com.google.gson.Gson;
import edu.uky.pml.ph.models.InfectionUpdate;
import edu.uky.pml.ph.models.CensusArea;
import io.cresco.library.data.TopicType;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.*;

@SuppressWarnings({"unused", "WeakerAccess"})
public class FakeGen {
    private PluginBuilder pluginBuilder;
    private LocationResolver locationResolver;
    private CLogger logger;
    private Timer generator = new Timer("Generator");
    private String siteId;

    public FakeGen(PluginBuilder pluginBuilder, LocationResolver locationResolver) {
        setPluginBuilder(pluginBuilder);
        setLocationResolver(locationResolver);
        this.logger = pluginBuilder.getLogger(FakeGen.class.getName(), CLogger.Level.Trace);
        setSiteId(pluginBuilder.getConfig().getStringParam("site_id", "1234"));
    }

    public void setPluginBuilder(PluginBuilder pluginBuilder) {
        this.pluginBuilder = pluginBuilder;
    }

    public void setLocationResolver(LocationResolver locationResolver) {
        this.locationResolver = locationResolver;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void start() {
        logger.trace("start()");
        if (generator == null)
            return;
        generator.scheduleAtFixedRate(new FakeGenTick(), 1000L, 5000L);
    }

    public void stop() {
        logger.trace("stop()");
        if (generator == null)
            return;
        generator.cancel();
    }

    class FakeGenTick extends TimerTask {
        private CLogger logger;
        public FakeGenTick() {
            this.logger = pluginBuilder.getLogger(FakeGenTick.class.getName(), CLogger.Level.Trace);
        }
        @Override
        public void run() {
            if (locationResolver != null) {
                CensusArea.Result area = locationResolver.getAreaResult();
                if (area != null) {
                    logger.trace("Tick from {} County, {} [FIPS: {}].", area.county_name, area.state_code, area.county_fips);
                    try {
                        Random random = new Random();
                        Gson gson = new Gson();
                        InfectionUpdate infectionUpdate = new InfectionUpdate();
                        infectionUpdate.setSiteId(siteId);
                        infectionUpdate.setFlu(random.nextInt(10) < 3 ? 1 : 0);
                        infectionUpdate.setHepA(random.nextInt(10) < 3 ? 1 : 0);
                        infectionUpdate.setHepB(random.nextInt(10) < 3 ? 1 : 0);
                        infectionUpdate.setHepC(random.nextInt(10) < 3 ? 1 : 0);
                        infectionUpdate.setArea(area);

                        TextMessage updateMsg = pluginBuilder.getAgentService().getDataPlaneService().createTextMessage();
                        updateMsg.setText(gson.toJson(infectionUpdate));
                        updateMsg.setStringProperty(InfectionUpdate.DATA_PLANE_IDENTIFIER_KEY, InfectionUpdate.DATA_PLANE_IDENTIFIER_VALUE);
                        pluginBuilder.getAgentService().getDataPlaneService().sendMessage(TopicType.AGENT, updateMsg);
                    } catch (JMSException e) {
                        logger.error("Error building update message: {}", e.getMessage());
                    }
                } else {
                    logger.error("Location has not been set.");
                }
            } else {
                logger.error("No location resolver has been configured.");
            }
        }
    }
}
